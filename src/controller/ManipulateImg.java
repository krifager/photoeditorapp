package controller;

import GUI.PhotoEditorGUI;

import java.awt.image.BufferedImage;
import java.util.Arrays;

public class ManipulateImg {

    public static void brightenImage(){
        BufferedImage newImg = PhotoEditorGUI.myImg;

        System.out.println("Brighten the image");
        //brighten the img
        int height = newImg.getHeight();
        int width = newImg.getWidth();
        int T = 30;//factor for brightness

        /*testing on a pixel
        int p = newImg.getRGB(height - (height/2),width-(width/2));
        System.out.println(p);System.out.println(" is the value of the pixel");
        // Components will be in the range of 0..255:
        int blue = p & 0xff;
        int green = (p & 0xff00) >> 8;
        int red = (p & 0xff0000) >> 16;
        System.out.println(blue);System.out.println(" is the value of blue");
        System.out.println(red);System.out.println(" is the value of red");
        System.out.println(green);System.out.println(" is the value of green");
        */

        for(int y=0; y<height; y++){
            for(int x=0; x<width; x++){
                int p = newImg.getRGB(x,y);
                int a= (p>>24)&0xff;
                int r= (p>>16)&0xff;
                int g= (p>>8)&0xff;
                int b= p&0xff;

                r+=T;if(r>255)r=255;
                g+=T;if(g>255)g=255;
                b+=T;if(b>255)b=255;

                p=(a<<24)|(r<<16)|(g<<8)|b;
                newImg.setRGB(x,y,p);
            }
        }

        PhotoEditorGUI.myImg = newImg;
    }

    public static void brightenReverseImage(){
        BufferedImage newImg = PhotoEditorGUI.myImg;

        System.out.println("Reverse brighten the image");
        //reverse brighten the img
        int height = newImg.getHeight();
        int width = newImg.getWidth();
        int T = 30;//factor for brightness

        for(int y=0; y<height; y++){
            for(int x=0; x<width; x++){
                int p = newImg.getRGB(x,y);
                int a= (p>>24)&0xff;
                int r= (p>>16)&0xff;
                int g= (p>>8)&0xff;
                int b= p&0xff;

                r-=T;if(r>255)r=255;
                g-=T;if(g>255)g=255;
                b-=T;if(b>255)b=255;

                p=(a<<24)|(r<<16)|(g<<8)|b;
                newImg.setRGB(x,y,p);
            }
        }
        PhotoEditorGUI.myImg = newImg;

    }

    public static void smoothenImage(){
        System.out.println("Smoothen the image");
        //smoothen the img
        int height = PhotoEditorGUI.myImg.getHeight();
        int width = PhotoEditorGUI.myImg.getWidth();
        int smoothenGridSize = 3*3;


        //go through every pixel
        for(int y=0; y<height; y++){
            for(int x=0; x<width; x++){
                int counter = 0;
                int p = PhotoEditorGUI.myImg.getRGB(x, y);
                int a= (p>>24)&0xff;

                int[] redPixelArray = new int[smoothenGridSize];
                int[] greenPixelArray = new int[smoothenGridSize];
                int[] bluePixelArray = new int[smoothenGridSize];
                //go through pixels in a 3x3 grid by starting at "x-1" and "y-1" and
                for(int i = (y-1); i<=(y+1); i++) {
                    for(int j = (x-1);j<=(x+1);j++) {
                        //check if the pixel is inside the grid
                        if(j >= 0 && j <= width && i >= 0 && i <= height) {
                            int tempPixel = PhotoEditorGUI.myImg.getRGB(x, y);

                            redPixelArray[counter] = (tempPixel >> 16) & 0xff;
                            greenPixelArray[counter] = (tempPixel >> 8) & 0xff;
                            bluePixelArray[counter] = tempPixel & 0xff;
                            counter++;
                        }
                    }
                }
                //sort the arrays
                Arrays.sort(redPixelArray,0,redPixelArray.length);
                Arrays.sort(greenPixelArray,0,greenPixelArray.length);
                Arrays.sort(bluePixelArray,0,bluePixelArray.length);

                //set new RGB
                int median = counter/2;
                int newRed = redPixelArray[median], newGreen = greenPixelArray[median], newBlue = bluePixelArray[median];
                p=(a<<24)|(newRed<<16)|(newGreen<<8)|newBlue;
                PhotoEditorGUI.myImg.setRGB(x,y,p);
            }
        }

    }


    public static void smoothenReverseImage(){
        BufferedImage newImg = PhotoEditorGUI.myImg;

        System.out.println("Reverse smoothen the image");
        //reverse smoothen the img
        int height = newImg.getHeight();
        int width = newImg.getWidth();



        PhotoEditorGUI.myImg = newImg;
    }

    public static void binaryImage(){
        BufferedImage newImg = PhotoEditorGUI.myImg;

        System.out.println("Make image black and white");
        //make img black n white
        int height = newImg.getHeight();
        int width = newImg.getWidth();

        for(int y=0; y<height; y++){
            for(int x=0; x<width; x++){
                int p = newImg.getRGB(x,y);

                int a= (p>>24)&0xff;
                int r= (p>>16)&0xff;
                int g= (p>>8)&0xff;
                int b= p&0xff;

                int avg = (r+b+g)/3;
                p=(a<<24)|(avg<<16)|(avg<<8)|avg;
                newImg.setRGB(x,y,p);
            }
        }

        PhotoEditorGUI.myImg = newImg;

    }

    public static void binaryReverseImage(){
        BufferedImage newImg = PhotoEditorGUI.myImg;

        System.out.println("Revert to colors");
        //reverse to colors
        int height = newImg.getHeight();
        int width = newImg.getWidth();


        PhotoEditorGUI.myImg = newImg;

    }

}
