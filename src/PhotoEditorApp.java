import GUI.PhotoEditorGUI;

import javax.swing.*;

public class PhotoEditorApp {

    public static int HEIGHT = 500;
    public static int LENGTH = 500;

    public static void main(String[] args){
        initGUI();//start the GUI
    }

    public static void initGUI(){
        PhotoEditorGUI newFrame = new PhotoEditorGUI();
        newFrame.setSize(LENGTH, HEIGHT);
        newFrame.setVisible(true);
        newFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}
