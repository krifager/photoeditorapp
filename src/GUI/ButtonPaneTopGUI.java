package GUI;

import controller.ManipulateImg;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;

public class ButtonPaneTopGUI extends JPanel {
    private JButton brightness;
    private JButton binary;
    private JButton smoothness;
    private JFrame frame;

    public ButtonPaneTopGUI(PhotoEditorGUI photoEditorFrame){
        frame = photoEditorFrame;
        //init buttons
        brightness = new JButton("Brightness");
        binary = new JButton("Binary");
        smoothness = new JButton("Smoothness");

        //place buttons on pane
        add(brightness);add(smoothness);add(binary);

        //set actions on buttons
        setBrightness();setBinary();setSmoothness();

    }

    public void setBrightness(){
        brightness.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                super.mousePressed(e);
                ManipulateImg.brightenImage();
                frame.repaint();
            }
        });

        brightness.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                super.mouseReleased(e);
                ManipulateImg.brightenReverseImage();
                frame.repaint();
            }
        });
    }

    public void setBinary(){
        binary.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                super.mousePressed(e);
                ManipulateImg.binaryImage();
                frame.repaint();
            }
        });

        binary.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                super.mouseReleased(e);
                ManipulateImg.binaryReverseImage();
                frame.repaint();
            }
        });
    }

    public void setSmoothness(){
        smoothness.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                super.mousePressed(e);
                ManipulateImg.smoothenImage();
                frame.repaint();
            }
        });

        smoothness.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                super.mouseReleased(e);
                ManipulateImg.smoothenReverseImage();
                frame.repaint();
            }
        });
    }
}
