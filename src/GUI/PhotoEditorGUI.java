package GUI;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class PhotoEditorGUI extends JFrame {

    private ButtonPaneTopGUI buttonPaneTopGUI;
    private JLabel label;
    public static BufferedImage myImg;


    //constructor
    public PhotoEditorGUI(){
        super("Photo editor");
        addTopPane();//add buttons top on screen

        //put image center screen
        myImg = loadImg(); label = new JLabel(new ImageIcon(myImg));
        getContentPane().add(label, BorderLayout.CENTER);
    }

    private void addTopPane(){
        buttonPaneTopGUI = new ButtonPaneTopGUI(this);//need so send this class so actions repaint after called
        add(buttonPaneTopGUI,BorderLayout.NORTH);//add top pane with buttons "brightness", "smoothness" and "binary" on top
    }

    private BufferedImage loadImg(){
        BufferedImage img = null;
        File f = null;

        try {
            f = new File("/home/prog/IdeaProjects/PhotoEditorApp/city-night.jpeg");
            img = ImageIO.read(f);
        }catch (IOException e){
            System.out.println(e);
        }

        return img;
    }

}
